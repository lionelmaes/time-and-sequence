<?php
require('config.php');
require('data.php');

ob_start();

?>

<!doctype html>
<html>

<head>
    <title>La Villa Hermosa := graphic and code design</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href='style/style.css' rel='stylesheet' type='text/css'>
</head>
<body id="viewer">
    <section class="intro">
        <?php echo '<p>'.$text.'</p>'; ?>
    </section>
    <section class="listing">

        <ul>

        <?php
            foreach($items as $i => $item):
        ?>
                        <li class="item<?php echo (isset($item['meta']['keywords']))?' '.$item['meta']['keywords']:''; ?>">
                            <a href="<?php echo $item['url'];?>"
                               data-extension="<?php echo $item['extension'];?>"
                               data-platform="<?php echo $item['platform'];?>"

                               <?php echo (isset($item['meta']['timer']))?'data-timing="'.$item['meta']['timer'].'"':''; ?>>
                                <?php echo $item['title']; ?>
                            </a>
                        </li>
        <?php
            endforeach;
        ?>
            </ul>

    </section>

    <main>
			<section class="stage">
			</section>
	</main>
	<footer>
        <span class="title"></span>
        <span class="timer"></span>
        <a href="#next" class="nextbtn">next</a> | <a href="#back" class="backbtn">back to intro</a>

    </footer>

</body>
<script src="https://player.vimeo.com/api/player.js"></script>
<script src="http://www.youtube.com/player_api"></script>
<script src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
<script src="js/edit.js" ></script>
</html>
<?php
    $data = ob_get_contents();
    $cacheData = preg_replace_callback('#href="(.*?'.str_replace('.', '\.', $host).'.*?)"#', function($matches){
        $fileName = pathinfo($matches[1], PATHINFO_BASENAME);
        if(!file_exists('./cache/files/'.$fileName)){
            backupFile($matches[1], dirname(__FILE__).'/cache/files/'.$fileName);
        }
        return 'href="cache/files/'.$fileName.'"';
    }, $data);

    file_put_contents('cache/viewer.html', $cacheData);
    ob_end_flush();

?>
