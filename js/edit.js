const $elements = document.querySelectorAll('.item');
const $stage = document.querySelector('section.stage');
const $items = document.querySelectorAll('section.listing a');
const $intro = document.querySelector('section.intro');
const $timer = document.querySelector('span.timer');
const $title = document.querySelector('span.title');
const $nextButton = document.querySelector('a.nextbtn');
const $backButton = document.querySelector('a.backbtn');
const $footer = document.querySelector('footer');
const autoplay = true;
let watch;

/////////////////////STAGE
function loadStage(item){

    closeStage();
    openStage();

    //format the timing
    var timing = [];
    if(item.getAttribute('data-timing') != null)
        timing = convertTiming(item.getAttribute('data-timing'));

    //console.log(timing);
    var extension = item.getAttribute('data-extension');

    $title.textContent = item.textContent;

    if(item.getAttribute('data-platform') == 'youtube' || item.getAttribute('data-platform') == 'vimeo'){
        loadHostedVideo(item.getAttribute('href'), timing, item.getAttribute('data-platform'));
    }else if(item.getAttribute('data-platform') == 'twitter'){
        loadTwitterFeed(item.getAttribute('href'), timing);

    }else{
        switch(extension){
            case 'jpg':
            case 'jpeg':
            case 'png':
            case 'gif':
                loadImage(item.getAttribute('href'), timing);
                break;
            case 'mp3':
                loadSound(item.getAttribute('href'), timing);
                break;
            case 'mp4':
                loadVideo(item.getAttribute('href'), timing);
                break;
            default:
                loadIframe(item.getAttribute('href'), timing);



        }


    }


}

function countdown(duration){
  let time = 0;
  setTimer(duration);
  watch = setInterval(function(){
      setTimer(duration - time);
      if(time >= duration){
          clearInterval(watch);
          closeStage();
          if(autoplay){
              $nextButton.click();
          }
      }
      time += 1;
  }, 1000);
}

function setTimer(time){

  if(time > 0)
    $timer.innerText = secToTimer(time)+' before ';
  else
    $timer.innerText = '';
}

function loadTwitterFeed(src, timing){
    $stage.innerHTML = '<a class="twitter-timeline" href="'+src+'"></a>';
    twttr.widgets.load();
    if(timing.length > 0){
        countdown(timing[0]);
    }

}
function loadIframe(src, timing){
    var $iframe = document.createElement('iframe');
    $iframe.setAttribute('src', src);
    $stage.appendChild($iframe);
    if(timing.length > 0){
        countdown(timing[0]);
    }
}

function loadImage(src, timing){
    var $image = document.createElement('img');
    $image.setAttribute('src', src);
    $stage.appendChild($image);
    if(timing.length > 0){
        countdown(timing[0]);
    }
}
function loadVideo(url, timing){
    var $player = document.createElement('video');
    $player.setAttribute('src', url);
    $player.setAttribute('controls', 'controls');
    $stage.appendChild($player);
    loadMedia($player, timing);
}
function loadSound(url, timing){
    var $player = document.createElement('audio');
    $player.setAttribute('src', url);
    $player.setAttribute('controls', 'controls');
    $stage.appendChild($player);
    loadMedia($player, timing);
}
function loadMedia($player, timing){
    $player.addEventListener('ended', function(){
        closeStage();
        if(autoplay){
            $nextButton.click();
        }
    });
    if(timing.length > 0){

        $player.addEventListener('canplay', function() {
            //the canplay event is fired when the currentTime proprety is changed so this creates a loop if not inside a condition
            if(this.currentTime < timing[0])
                $player.currentTime = timing[0];

        });

    }if(timing.length > 1){
        setTimer(timing[1]-timing[0]);
        watch = setInterval(function(){
            setTimer(timing[1]-$player.currentTime);
            if($player.currentTime >= timing[1]){

                $player.pause();
                clearInterval(watch);
                closeStage();
                if(autoplay){
                    $nextButton.click();
                }
            }
        }, 1000);
    }

    $player.play();
}
function loadHostedVideo(url, timing, platform){

    var $player = document.createElement('div');
    var $playerContainer = document.createElement('div');
    $playerContainer.setAttribute('class', 'video-container');
    $playerContainer.appendChild($player);
    $player.setAttribute('id', 'hplayer');
    $stage.appendChild($playerContainer);

    switch(platform){
        case 'youtube':
            var id = new URL(url).searchParams.get('v');
            if(YT.player != undefined) var waitForYT = 0;
            else waitForYT = 3000;

            setTimeout(function(){
                var player = new YT.Player('hplayer', {
                  width: '640',
                  height: '390',

                  videoId: id,
                  events: {
                    onReady: onYTPlayerReady,
                    onStateChange: onYTPlayerStateChange
                  }
                });
                player.timing = timing;
            }, waitForYT);
            break;
        case 'vimeo':
            var id = url.substr(url.lastIndexOf('/') + 1);

            var player = new Vimeo.Player('hplayer', {
                width: '640',
                height: '390',

                id:id
            });
            player.on('loaded', onVimeoPlayerReady);
            player.on('ended', onVimeoPlayerEnd);
            player.timing = timing;

    }



}
function onVimeoPlayerReady(event){
    var player = this;
    if(player.timing.length > 0)
        player.setCurrentTime(player.timing[0]);
    if(player.timing.length > 1){
        setTimer(player.timing[1] - player.timing[0]);
        watch = setInterval(function(){

            player.getCurrentTime().then(function(seconds) {
                setTimer(player.timing[1] - seconds);
                if(seconds >= player.timing[1]){

                    player.pause();
                    clearInterval(watch);
                    closeStage();
                    if(autoplay){

                        $nextButton.click();
                    }
                }
            }).catch(function(error) {
                console.log(error);
            });


        }, 1000);
    }
    player.play();

}

function onYTPlayerReady(event) {
    if(event.target.timing.length > 0)
        event.target.seekTo(event.target.timing[0]);

    if(event.target.timing.length > 1){
        console.log(event.target.timing[1] - event.target.timing[0]);
        setTimer(event.target.timing[1] - event.target.timing[0]);
        watch = setInterval(function(){
            console.log(event.target.timing[1] - event.target.getCurrentTime());
            setTimer(event.target.timing[1] - event.target.getCurrentTime());
            if(event.target.getCurrentTime() >= event.target.timing[1]){

                event.target.stopVideo();
                event.target.destroy();
                clearInterval(watch);
                closeStage();
                if(autoplay){

                    $nextButton.click();
                }
            }

        }, 1000);
    }
    event.target.playVideo();
}

// when video ends
function onYTPlayerStateChange(event) {
    if(event.data === 0) {
        event.target.destroy();
        closeStage();
        if(autoplay){

            $nextButton.click();
        }
    }
}
function onVimeoPlayerEnd(e){
    closeStage();
    if(autoplay){
        $nextButton.click();
    }
}

function openStage(){

    $stage.classList.add('active');
    $stage.style.display = "block";

}

function closeStage(){
    if(watch != null)
      clearInterval(watch);
    $stage.innerHTML = '';
    $stage.classList.remove('active');
    $stage.style.display = "none";

}





//////////////////////EVENTS NAVIGATION
function updateList(){

    var activeKeywords = [];
    for(var i = 0; i < $keywords.length; i++){
        if($keywords[i].classList.contains('active'))
            activeKeywords.push($keywords[i].getAttribute('href').substr(1));
    }
    elementsLoop:
    for(var i = 0; i < $elements.length; i++){
        keywordsLoop:
        for(var j = 0; j < activeKeywords.length; j++){
            if($elements[i].classList.contains(activeKeywords[j])){
                $elements[i].classList.add('active');
                continue elementsLoop;
            }
        }
        $elements[i].classList.remove('active');
    }
}


function initEvents(){
    $intro.addEventListener('click', function(e){
        this.style.display = 'none';
        $footer.style.display = 'block';
        $nextButton.click();
    }
    );

    for(let i = 0; i < $items.length; i++){
        $items[i].addEventListener('click', function(e){
            e.preventDefault();
            loadStage(this);
        });
    }

    let itemI = 0;
    if($nextButton != undefined){
        $nextButton.addEventListener('click', function(e){
            e.preventDefault();

            $items[itemI].click();
            itemI++;
            if(itemI >= $items.length)
                itemI = 0;

        });
    }
    if($backButton != undefined){
      $backButton.addEventListener('click', function(e){

          e.preventDefault();
          itemI--;
          setTimer(0);
          closeStage();
          $intro.style.display = 'block';
          $footer.style.display = 'none';
      });
    }


}

////////////////////////////UTILS
//convert from hours:minutes:seconds format to number of seconds
function convertTiming(timingstr){
    var timingParts = timingstr.split('-');
    var timing = [];
    for(var i in timingParts){
        var time = timingParts[i].split(':');
        if(time.length == 1)
            seconds = parseFloat(time[0]);
        else if(time.length == 2){
            seconds = parseFloat(time[0]) * 60 + parseFloat(time[1]);
        }else if(time.length == 3){
            seconds = parseFloat(time[0]) * 60 * 60 + parseFloat(time[1]) * 60 + parseFloat(time[2]);
        }

        timing.push(seconds);



    }
    return timing;


}

/* HH:MM:SS.MS to (FLOAT)seconds ---------------*/
function timerToSec(timer){
   let vtimer = timer.split(":")
   let vhours = +vtimer[0]
   let vminutes = +vtimer[1]
   let vseconds = parseFloat(vtimer[2])
   return vhours * 3600 + vminutes * 60 + vseconds
}

/* Seconds to (STRING)HH:MM:SS.MS --------------*/
function secToTimer(sec){
  let o = new Date(0)
  let p =  new Date(sec*1000)
  return new Date(p.getTime()-o.getTime())
    .toISOString()
    .split("T")[1]
    .split(".")[0]
}

function shufflenl(nl){
    var arr = [];
    for(let i = nl.length; i--; arr.unshift(nl[i]));
    shuffle(arr);
    return arr;

}
function shuffle(a) {
    for (let i = a.length - 1; i > 0; i--) {
        const j = Math.floor(Math.random() * (i + 1));
        [a[i], a[j]] = [a[j], a[i]];
    }
    return a;
}

initEvents();
