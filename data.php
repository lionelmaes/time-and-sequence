<?php

ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);
error_reporting(E_ALL);



$host = parse_url($padAddr, PHP_URL_HOST);

function backupFile($file_url, $save_to){
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_POST, 0);
        curl_setopt($ch,CURLOPT_URL,$file_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $file_content = curl_exec($ch);
        curl_close($ch);

        $downloaded_file = fopen($save_to, 'w');
        fwrite($downloaded_file, $file_content);
        fclose($downloaded_file);
}


function preProcess($data) {
    //$data = utf8_decode($data);
    $output = array();
    $lineregex = '/^\[(.*?)\]\((.*?)\)/m';

    //first extract data parts from data
    preg_match_all('/###(.*?)\\n(.*?)###/sm', $data, $matches);

    foreach($matches[1] as $i => $type){
      if($type != 'sequence'){
        $output[$type] = $matches[2][$i];
      }else{
        $output[$type] = array();
        preg_match_all($lineregex, $matches[2][$i], $lines);
        $items = array();

        foreach($lines[0] as $j => $line){

          $extension = pathinfo(parse_url($lines[2][$j])['path'], PATHINFO_EXTENSION);
          if($extension == '' && preg_match('#youtu#', $lines[2][$j]))
              $platform = 'youtube';
          else if($extension == '' && preg_match('#vimeo#', $lines[2][$j]))
              $platform = 'vimeo';
          else if($extension == '' && preg_match('#twitter#', $lines[2][$j]))
              $platform = 'twitter';
          else
              $platform = '';

          $meta = array();

          $detail = explode('_', $lines[1][$j]);


          if(count($detail) < 2)
              continue;

          $title = $detail[0];

          if(count($detail) == 2){
            $meta['timer'] = $detail[1];
          }else if(count($detail) == 3){
            $meta['keywords'] = strtolower(str_replace(array(' ', '-'), array('_', ' '), $detail[1]));
            $meta['timer'] = $detail[2];
          }else{
            $meta['keywords'] = strtolower(str_replace(array(' ', '-'), array('_', ' '), $detail[1]));
            $meta['category'] = strtolower($detail[2]);
            $meta['timer'] = $detail[3];
          }



          /*$currentKeywords = explode(' ', $meta[1]);

          foreach($currentKeywords as $keyword){
              if(!in_array($keyword, $keywords)){
                  $keywords[] = $keyword;
              }
          }*/
          $item = array('title'=>$title, 'url'=>$lines[2][$j], 'meta'=>$meta, 'extension'=>$extension, 'platform'=>$platform);
          $output[$type][] = $item;


        }





      }
    }

    return $output;
}


$src = file_get_contents($padAddr.'/export/txt');
if($src === false || $tunnel === false){

    include('cache/viewer.html');
    exit();

}
$proc = preProcess($src);

$items = $proc['sequence'];

$text = $proc['intro'];


?>
